# -*- coding: utf-8 -*-
from re import sub, Match
from typing import Iterator

from loguru import logger

from repair.dir_file.dir_file import TextFile
from repair.general.errors import InvalidMatchError, FileInvalidTypeError

__all__ = ["InternalLinkInspector", "internal_inspector"]


def _replace_dash_underline(match: Match) -> str:
    """
    The replacement of snake_case and kebab-case to camelCase.

    Parameters
    ----------
    match : Match
        The substring matching the pattern.

    Returns
    -------
    str
        The updated string converted to camelCase.

    Raises
    ------
    InvalidMatchError
        If the line does not have dashes or underlines in proper places.

    """
    if not match:
        logger.error(f"В строке не найден паттерн {match.re.pattern}")
        raise InvalidMatchError
    else:
        return str(match.group(2).upper())


def _iter_internal_options(line: str) -> Iterator[str]:
    """
    The iterator for updated forms of line.

    The updated forms are snake_case, kebab-case, and camelCase.

    Parameters
    ----------
    line : str
        The line to switch the case.

    Returns
    ------
    Iterator[str]
        The new form of line converted to one of the cases.

    """
    to_underline: str = line.replace("-", "_")
    to_dash: str = line.replace("_", "-")
    to_camel_case: str = sub("([-_])(\\w)", _replace_dash_underline, line)

    return iter((to_underline, to_dash, to_camel_case))


class InternalLinkInspector:
    """
    The inspector for internal links having only anchors.

    Attributes
    ----------
    _text_file : TextFile or None
        The markdown file to inspect internal links.

    """

    def __init__(self):
        self._text_file: TextFile | None = None

    def __str__(self):
        return f"{self.__class__.__name__}()"

    def __repr__(self):
        return f"<{self.__class__.__name__}()>"

    @property
    def text_file(self) -> TextFile:
        return self._text_file

    @text_file.setter
    def text_file(self, value):
        if isinstance(value, TextFile):
            self._text_file = value
        else:
            logger.error(f"Файл должен быть типа Text, но получено {type(value)}")
            raise FileInvalidTypeError

    def modified_anchor(self, anchor: str) -> str | None:
        """
        The possible modified original anchor.

        Attributes
        ----------
        anchor : str
            The original anchor from the link.

        Returns
        -------
        str
            The modified anchor found in the file.

        """
        for _modified_anchor in _iter_internal_options(anchor):
            if _modified_anchor in self._text_file.iter_anchors():
                return _modified_anchor
        else:
            return

    @property
    def _dir_file_anchors(self) -> set[str]:
        """
        The anchors in the file.

        Returns
        -------
        set[str]
            The found anchors in the file as is.

        """
        return set(self._text_file.iter_anchors())

    def _missing_anchors(self) -> set[str]:
        """
        The anchors that are not found in the file.

        Returns
        -------
        set[str]
            The bad internal links in the file.

        """
        _link_anchors: set[str] = set(self._text_file.iter_internal_link_anchors())

        return _link_anchors.difference(self._dir_file_anchors)

    def inspect_links(self):
        """
        The validation and update of the internal links.

        """
        if not self._missing_anchors():
            logger.debug("All internal links and anchors are valid")
            return

        for _anchor in self._missing_anchors():
            _modified_anchor: str | None = self.modified_anchor(_anchor)

            if _modified_anchor is not None:
                logger.warning(
                    f"В файле {self._text_file.rel_path} найден якорь\n"
                    f"{_modified_anchor} вместо {_anchor}", result=True)

                for _i_link in self._text_file.get_internal_links(_anchor):
                    self._text_file.update_line(_i_link.index, _i_link.anchor, _modified_anchor)

                logger.success(
                    f"Файл {self._text_file.rel_path}:\n"
                    f"{_anchor} -> {_modified_anchor}\n", result=True)

            else:
                _indexes: str = ", ".join(
                    str(_internal_link.index) for _internal_link in self._text_file.get_internal_links(_anchor))
                logger.error(
                    f"Не найден якорь {_anchor} в файле {self._text_file.rel_path}.\n"
                    f"Строки: {_indexes}", result=True)


internal_inspector: InternalLinkInspector = InternalLinkInspector()
internal_inspector.__doc__ = "The default internal link inspector."

# -*- coding: utf-8 -*-
from repair.inspector.anchor_inspector import AnchorInspector
from repair.inspector.internal_link_inspector import InternalLinkInspector, internal_inspector
from repair.inspector.link_fixer import LinkFixer, link_fixer
from repair.inspector.link_inspector import LinkInspector, link_inspector

#!/usr/bin/env bash

if [ "${OSTYPE::4}" == "msys" ]
then
  FILE="link_repair.exe.spec"
  DIRECTORY="$HOME"\PycharmProjects\link_repair\repair\
  poetry shell || echo "Already activated"
else
  FILE="link_repair.spec"
  DIRECTORY="$HOME"/PycharmProjects/link_repair/repair/
fi

cd "$DIRECTORY" || echo "No directory found"
poetry run pyinstaller --distpath="../bin" --noconfirm --log-level="DEBUG" "$FILE" 2> ../pyinstaller.log